## Running

Invoke this program passing the name of the SQLite database file as the single argument:

    cargo run resources/test-questions.db

To see usage options pass the `-h` switch like this:

    cargo run -- -h

**In order to save your progress you must exit the program by typing the letter `'q'` to quit.**

## Where did the questions come from?

The database of questions was created, mostly interactively, using the Scala code in the `extractor` directory.  We expect it will be unneeded until the Ministry changes the content of the questions on its website.

## Theory of Question Repetition

The goal is to identify the more difficult items, and repeat those the most.  Factors to determine difficulty are:

* Whether the user answered this item wrong in the past,
* The amount of time the user took to answer in the past,

The question that is asked next, is randomly selected from all the items that are tied for the top priority.  Priory rules include:

* Any never-asked question always has higher priority than every asked question.
* A question answered correctly the last time asked has priority over all questions answered wrong the last time asked.
* When two questions that were both last answered wrong or both last answered right are compared, the question that took the longest to answer takes priority.
* When comparing items equal according to the foregoing rules, the question that was asked longer ago takes priority over the more recently asked question.

So the information needed to compare to items for ask-priority are

* Has this question ever been asked?
* If so, was it correct?
* How long ago was it asked?
* How long did it take to answer.


