use chrono::{DateTime, Duration, Local, Utc};
use clap::Parser;
use console::Term;
use rand::rngs::ThreadRng;
use rand::Rng;
use rusqlite::{Connection, Error as SqlError, OpenFlags, Row};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::fs;
use std::io::Error as IoError;
use std::path::{Path, PathBuf};
use std::process;

#[derive(Debug)]
struct Item {
    section: String,
    question: String,
    image: Option<String>,
    correct_answer: String,
    wrong_answer_1: String,
    wrong_answer_2: String,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
struct Meta {
    last_time: DateTime<Utc>,
    last_correct: bool,
    last_duration: DurationWrapper,
}

#[derive(Parser)]
#[command(author,version,about,long_about=None)]
struct Cli {
    /// Name of the sqlite3 database file containing the test questions
    db_filename: PathBuf,

    /// Word to filter the questions by
    #[arg(short, long, value_name = "FILTER-TERM")]
    filter: Option<String>,

    /// Ignore existing state from any previous game
    #[arg(short, long)]
    newgame: bool,

    /// Choose questions randomly rather than always the oldest wrongly-answered one.
    #[arg(short, long)]
    randomize: bool,

    /// Turn debugging information on
    #[arg(short,long,action = clap::ArgAction::Count)]
    debug: u8,
}

fn main() {
    let cli = Cli::parse();
    let db_filename = cli.db_filename;
    println!("Value for db_filename: {:?}", db_filename);

    //let db_path = Path::new(&db_filename);
    let db_path = db_filename.canonicalize().unwrap();

    let image_pathbuf = db_path.parent().unwrap().join("images");
    let image_path = image_pathbuf.as_path();

    let items = match read_items(db_path.to_str().unwrap(), cli.filter) {
        Ok(items) => items,
        Err(err) => {
            eprintln!("{}", err);
            process::exit(1);
        }
    };

    let item_count = items.len();

    println!("{} items", item_count);

    let mut rng = rand::thread_rng();
    let mut state: HashMap<u16, Meta> = match cli.newgame {
        false => read_state(Path::new("state.json")),
        true => HashMap::new(),
    };
    let term: console::Term = Term::stdout();

    loop {
        match ask(&mut rng, &items, image_path, &state, cli.randomize) {
            Ok((id, meta)) => {
                state.insert(id, meta);
                display_status(&items, &state);
                let _ = term.write_line("Press any key to continue.");
                let c = {
                    match term.read_char() {
                        Ok(c) => c,
                        _ => break,
                    }
                };
                let _ = std::process::Command::new("killall")
                    .arg("gwenview")
                    .output();
                if c == 'q' {
                    break;
                }
            }
            _ => {
                break;
            }
        }
    }

    println!("\nSUMMARY\n");
    display_status(&items, &state);
    match fs::write("state.json", serde_json::to_string_pretty(&state).unwrap()) {
        Err(e) => eprintln!("failed to save state: {}", e),
        Ok(()) => (),
    }
}

fn display_status(items: &HashMap<u16, Item>, state: &HashMap<u16, Meta>) {
    let total_count = items.len();
    let asked_count = state.len();
    let unasked_count = total_count - asked_count;
    println!(
        "{} total questions; {} seen, {} unseen",
        total_count, asked_count, unasked_count,
    );
    //    let (right, wrong): (Values<u16, Meta>, Values<u16, Meta>) = {
    let (right, wrong): (Vec<Meta>, Vec<Meta>) = {
        let values = state.values();
        values.partition(|m| m.last_correct)
    };
    let right_count: f64 = right.len() as f64;
    let wrong_count: f64 = wrong.len() as f64;
    println!(
        "{right_count} right, {wrong_count} wrong ({:.0}%)",
        (100.0 * right_count) / (right_count + wrong_count)
    );

    /*println!("STATE AS IN MEMORY:");
    println!("\n{:?}", state);
    println!("STATE AS TO BE WRITTEN:");
    println!("\n{:?}", serde_json::to_string_pretty(&state));*/
}

fn read_state(filepath: &Path) -> HashMap<u16, Meta> {
    match fs::read_to_string(filepath) {
        Ok(json_string) => match serde_json::from_str(&json_string) {
            Ok(hash_map) => hash_map,
            Err(msg) => {
                eprintln!(
                    "deserializing state file {} failed. Resetting game state. {msg}",
                    filepath.to_str().unwrap_or("(?)")
                );
                HashMap::new()
            }
        },
        Err(msg) => {
            eprint!("no state file read: {msg}");
            HashMap::new()
        }
    }
}

fn read_items(db_path: &str, filter: Option<String>) -> Result<HashMap<u16, Item>, SqlError> {
    let conn = Connection::open_with_flags(db_path, OpenFlags::SQLITE_OPEN_READ_ONLY)?;

    let mut stmt = conn.prepare(
        &(r#"SELECT id, s.sectionTitle, question, image,
                  correctAnswer, wrongAnswer1, wrongAnswer2
           FROM questions q INNER JOIN sections s ON q.sectionNumber = s.sectionNumber"#
            .to_owned()
            + (match filter {
                None => "",
                Some(_) => " WHERE question LIKE ?1 OR correctAnswer LIKE ?2 OR wrongAnswer1 LIKE ?3 OR wrongAnswer2 LIKE ?4",
            })
            .as_ref()),
    )?;

    let map_func = |row: &Row| {
        let index: u16 = row.get(0)?;
        Ok((
            index,
            Item {
                section: row.get(1)?,
                question: row.get(2)?,
                //image: row.get(3).ok(),
                image: row.get::<_, Option<String>>(3)?,
                correct_answer: row.get(4)?,
                wrong_answer_1: row.get(5)?,
                wrong_answer_2: row.get(6)?,
            },
        ))
    };

    let rows = match filter {
        None => {
            println!("no params");
            stmt.query_map([], map_func)
        }
        Some(term) => {
            println!("two param\n{:?}", stmt);
            let like_term = "%".to_owned() + &term + "%";
            stmt.query_map(
                (
                    like_term.clone(),
                    like_term.clone(),
                    like_term.clone(),
                    like_term.clone(),
                ),
                map_func,
            )
        }
    }?;

    let mut items = HashMap::new();

    for item_result in rows {
        let (index, item) = item_result?;
        items.insert(index, item);
    }
    Ok(items)
}

fn ask(
    rng: &mut ThreadRng,
    items: &HashMap<u16, Item>,
    images_dir: &Path,
    state: &HashMap<u16, Meta>,
    randomize: bool,
) -> Result<(u16, Meta), IoError> {
    let seen_item_keys: HashSet<&u16> = state.keys().collect();
    let all_item_keys: HashSet<&u16> = items.keys().collect();
    let unseen_item_keys: Vec<&u16> = all_item_keys
        .difference(&seen_item_keys)
        .into_iter()
        .map(|x| *x)
        .collect();

    // If there are any unseen items, we will choose one randomly.
    // If all items have been seen, we choose the one that was last answered wrongly the longest time ago.
    let item_id: &u16 = {
        if unseen_item_keys.is_empty() {
            let x = state.iter().filter(|(_, meta)| !meta.last_correct);
            if randomize {
                // choose a random wrongly-answered item
                let c: Vec<(&u16, &Meta)> = x.collect();
                c.get(rng.gen_range(0..c.len())).unwrap().0
            } else {
                // choose the oldest wrongly-answered item
                x.reduce(|acc, elem| {
                    if acc.1.last_time > elem.1.last_time {
                        elem
                    } else {
                        acc
                    }
                })
                .unwrap()
                .0
            }
        } else {
            // choose a random unseen item
            unseen_item_keys[rng.gen_range(0..unseen_item_keys.len())]
        }
    };

    let item = match items.get(item_id) {
        Some(i) => i,
        None => {
            eprintln!("failed to get {item_id}");
            process::exit(4);
        }
    };

    let first_draw = rng.gen_range(0..=2);
    let second_draw = rng.gen_range(0..=1);

    let (first, second, third, correct) = {
        if first_draw == 0 {
            if second_draw == 0 {
                (
                    item.correct_answer.clone(),
                    item.wrong_answer_1.clone(),
                    item.wrong_answer_2.clone(),
                    'a',
                )
            } else {
                (
                    item.correct_answer.clone(),
                    item.wrong_answer_2.clone(),
                    item.wrong_answer_1.clone(),
                    'a',
                )
            }
        } else if first_draw == 1 {
            if second_draw == 0 {
                (
                    item.wrong_answer_1.clone(),
                    item.correct_answer.clone(),
                    item.wrong_answer_2.clone(),
                    'b',
                )
            } else {
                (
                    item.wrong_answer_2.clone(),
                    item.correct_answer.clone(),
                    item.wrong_answer_1.clone(),
                    'b',
                )
            }
        } else {
            if second_draw == 0 {
                (
                    item.wrong_answer_1.clone(),
                    item.wrong_answer_2.clone(),
                    item.correct_answer.clone(),
                    'c',
                )
            } else {
                (
                    item.wrong_answer_2.clone(),
                    item.wrong_answer_1.clone(),
                    item.correct_answer.clone(),
                    'c',
                )
            }
        }
    };

    let term: console::Term = Term::stdout();
    let _ = term.clear_screen();
    println!("({}) {}\n", item_id, item.question);
    item.image.as_ref().map(|filename| {
        let image_path = images_dir
            .join(filename)
            .into_os_string()
            .into_string()
            .unwrap();
        open::that_in_background(image_path);
    });
    println!("\ta. {}", first);
    println!("\tb. {}", second);
    println!("\tc. {}\n", third);
    let start_time = Local::now().time();

    let char = term.read_char()?;
    if char != 'a' && char != 'b' && char != 'c' {
        println!("try again");
    }
    let end_time = Local::now().time();
    let last_duration: Duration = end_time - start_time;
    let correct_sentence = item.question.clone()
        + " "
        + match correct {
            'a' => &first,
            'b' => &second,
            'c' => &third,
            _ => process::exit(3),
        }
        + "\n("
        + &item.section
        + ")";

    let last_correct: bool = {
        if char == correct {
            println!("CORRECT!\n\n\t{correct_sentence}\n");
            true
        } else {
            println!("WRONG!\n\n\t{correct_sentence}\n");
            false
        }
    };
    Ok((
        *item_id,
        Meta {
            last_time: Utc::now(),
            last_correct: last_correct,
            last_duration: DurationWrapper::from(last_duration),
        },
    ))
}

// From ChatGPT -- why is this stuff not in the Chrono library???

#[derive(Serialize, Deserialize, Debug)]
struct DurationWrapper {
    seconds: i64,
    nanoseconds: i32,
}

impl From<Duration> for DurationWrapper {
    fn from(duration: Duration) -> Self {
        DurationWrapper {
            seconds: duration.num_seconds(),
            nanoseconds: duration.num_nanoseconds().unwrap_or(0) as i32,
        }
    }
}

impl Into<Duration> for DurationWrapper {
    fn into(self) -> Duration {
        Duration::seconds(self.seconds) + Duration::nanoseconds(i64::from(self.nanoseconds))
    }
}

// end of ChatGPT code

impl Copy for DurationWrapper {}

impl Clone for DurationWrapper {
    fn clone(&self) -> Self {
        DurationWrapper {
            seconds: self.seconds,
            nanoseconds: self.nanoseconds,
        }
    }
}
