CREATE TABLE sections (
  sectionNumber integer PRIMARY KEY,
  sectionTitle text NOT NULL
);

CREATE TABLE questions (
  id integer PRIMARY KEY,
  sectionNumber integer NOT NULL,
  question text NOT NULL,
  image text,
  correctAnswer text NOT NULL,
  wrongAnswer1 text NOT NULL,
  wrongAnswer2 text NOT NULL,
  FOREIGN KEY(sectionNumber) REFERENCES sections(sectionNumber)
);
