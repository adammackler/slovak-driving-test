import io.circe._, io.circe.parser._
import io.circe.generic.auto._, io.circe.syntax._
import java.net.URLEncoder
import java.nio.file.{Files, Path, Paths}
import java.nio.charset.StandardCharsets.UTF_8

/*
    Get images:
    https://www.minv.sk/egovinet02/PCPZobrazFile?fileName=pcpfiles/<filename>
 */

object Main:

  def writeFile(filename: String, json: Json): Path =
    Files.write(
      Paths.get(filename),
      json.spaces2.getBytes(UTF_8)
    )

  def main(args: Array[String]) =
    val (questions, sections) = process
    val questionPath =
      writeFile("questions.json", questions.asJson)
    println(s"wrote questions to $questionPath")
    val sectionPath =
      writeFile("sections.json", sections.asJson)

    println(s"wrote sections to $sectionPath")

  case class Item(
    id: Int,
    section: Int,
    question: String,
    image: Option[String],
    correctAnswer: String,
    wrongAnswer1: String,
    wrongAnswer2: String
  )

  /** Take a map of section-number -> number-of-first-question-in-that-section,
    * and a question number.
    * return the section number of that question */
  @annotation.tailrec
  def questionSection(sectionStarts: List[(Int, Int)], questionNum: Int): Int =
    sectionStarts match
      case Nil => throw new Exception
      case (sectionNum, startNum) :: tail =>
        questionNum >= startNum match
          case true => sectionNum
          case false => questionSection(tail, questionNum)

  /** Return a Vector containing all test items, and a structure with the section names */
  def process: (Vector[Item], Map[Int, String]) =
    val rawJson = scala.io.Source.fromFile("src/main/resources/data.json").mkString
    val parseResult = parse(rawJson)
    val json = parseResult match
      case Left(e) => throw(e)
      case Right(json) => json
    val tests: Vector[JsonObject] = json.asArray.get.map(_.asObject.get)
    assert(tests.length == 100)

    val Empty = ""
    val sectionNames = collection.mutable.Map.empty[Int, String]
    def confirmSectionNames(sectionNames: collection.mutable.Map[Int,String], newSections: Map[String,Json]): Unit =
      newSections.map: (k,v) =>
        (k.toInt, v.asArray.get.apply(0).asObject.get.apply("txt").get.asString.get)
      .foreachEntry: (number: Int, name: String) =>
        sectionNames.get(number) match
          case None => sectionNames.addOne(number -> name)
          case Some(previous) => assert(name == previous)

    val itemsByTest: Vector[Vector[Item]] = tests.map: (test: JsonObject) =>
      val questions: JsonObject = test("otazky").get.asObject.get
      val answers: JsonObject = test("odpovede").get.asObject.get
      assert(questions.size == 40 && answers.size == 40)

      val sections: Map[String, Json] = test("okruhy").get.asObject.get.toMap
      confirmSectionNames(sectionNames, sections)
      // number of the first question of each section, in descending order
      val sectionStarts: List[(Int, Int)] = sections.toList.sortBy(_._1).reverse.map: (sec,json) =>
        val qNum = json.asArray.get(0).asObject.get.apply("zacina").get.asNumber.get.toInt.get
        (sec.toInt, qNum)

      val items = questions.keys.map: (key: String) =>
        val q0: Vector[Json] = questions(key).get.asArray.get
        assert(q0.length == 1)
        val q: JsonObject = q0(0).asObject.get
        val id = q("id").get.asNumber.get.toInt.get
        val options: Vector[String] = answers(key).get.asArray.get.map(_.asObject.get("odpoved").get.asString.get)
        assert(options.length == 3)
        val correctIndex = q("platna").get.asNumber.get.toInt.get - 1
        val incorrectAnswers = options.zipWithIndex.filter((a,i) => i != correctIndex).map(_._1)
        val image: Option[String] = q("obrazok").get.asString match
          case Some(Empty) => None
          case Some(s) => Some(s.replaceAll("/", ","))

        Item(
          id = id,
          section = questionSection(sectionStarts, key.toInt),
          question = q("text").get.asString.get,
          image = image,
          correctAnswer = options(correctIndex),
          wrongAnswer1 = incorrectAnswers(0),
          wrongAnswer2 = incorrectAnswers(1),
        )
      items.toVector
    val allItems: Vector[Item] = itemsByTest.flatten

    val grouped: Map[Int, Vector[Item]] = allItems.groupBy(_.id)
    val itemIds: Vector[Int] = grouped.keySet.toVector.sorted

    itemIds.map: id =>
      (id, grouped(id).length)



    val dedup: Vector[Item] = itemIds.map: id =>
      val is = grouped(id)
      is.tail.fold(is.head): (a,b) =>
        assert(a.id == b.id)
        assert(a.question == b.question)
        assert(a.image == b.image)
        assert(a.correctAnswer == b.correctAnswer)
        assert(a.wrongAnswer1 == b.wrongAnswer1 || a.wrongAnswer1 == b.wrongAnswer2)
        assert(a.wrongAnswer2 == b.wrongAnswer2 || a.wrongAnswer2 == b.wrongAnswer1)
        a

    (dedup, sectionNames.toMap)
