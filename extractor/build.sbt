scalaVersion := "3.3.1"
libraryDependencies ++= Seq(
  "io.circe" %% "circe-parser" % "0.14.6",
  "io.circe" %% "circe-generic" % "0.14.6"
)
